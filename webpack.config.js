
const path = require('path');
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const cssExtractor = new ExtractTextPlugin("style/style.bundle.css");

module.exports = {
    devtool: "source-map",
    entry: `${__dirname}/src/index.js`,
    output: {
        path: `${__dirname}/dist/`,
        filename: 'bundle.js',
    },

    module:{
        loaders:[

            { test: /\.(png|jpg|woff|woff2|eot|ttf|svg)$/, loader: 'url?limit=100000' },

            {
                test: /\.scss$/,
                loader: cssExtractor.extract("style",`css!postcss!sass`),
            },

            {
                test: /\.css$/,
                loader: cssExtractor.extract("style",`!css!postcss`)
            },

            {
                test: /\.jsx?$/,
                loaders: ["babel"],
                include: [ `${__dirname}/src/` ]
            }
        ]
    },

    postcss: function(){
        return [autoprefixer];
    },

    plugins: [
        cssExtractor,
        new webpack.optimize.DedupePlugin()
    ]

};